from typing import Optional

from pydantic import BaseModel


class UserModel(BaseModel):
    name: str
    surname: str
    rendering_name: str
    email: str


class UserModelPrivate(UserModel):
    password: str
    address: Optional[str]


class UserModelGet(UserModel):
    id: int


class UserRequestModel(BaseModel):
    id: Optional[int]
    name: Optional[str]
    surname: Optional[str]
    rendering_name: Optional[str]
    email: Optional[str]
