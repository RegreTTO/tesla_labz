import os

BD_USERNAME = os.environ.get("POSTGRES_USER", "admin")
BD_PASSWORD = os.environ.get("POSTGRES_PASSWORD", "admin")
BD_NAME = os.environ.get("POSTGRES_NAME", "database")
