from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session

from tesla.core.controllers import get_all_users_controller
from tesla.core.controllers.user_controllers import get_user_by_id_controller
from tesla.core.middlewares import generate_session
from tesla.core.schemas.user_schemas import UserModel
from tesla.core.store.db_model import User

user_router = APIRouter()


@user_router.get('/api/users', response_model=UserModel)
def get_all_users(session: Session = Depends(generate_session)):
    return get_all_users_controller(session=session)


@user_router.get('/api/user/{user_id}', response_model=UserModel)
def get_user_by_id(user_id: int, session: Session = Depends(generate_session)) -> User:
    return get_user_by_id_controller(user_id, session=session)
