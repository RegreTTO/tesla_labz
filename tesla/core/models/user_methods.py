from typing import List

from sqlalchemy.orm import Session

from tesla.core.schemas.user_schemas import UserRequestModel
from tesla.core.store.db_model import User


def get_all_users_from_database(session: Session) -> List[User]:
    users = session.query(User).all()
    return users


def get_user_from_database(user: UserRequestModel, session: Session) -> User:
    user = session.query(User).filter_by(**user.dict()).first()
    return user
