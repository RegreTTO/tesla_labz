"""Модуль, который содержит методы авторизации"""
from datetime import datetime, timedelta
import os

from fastapi import Depends, HTTPException
from fastapi.security import OAuth2PasswordBearer
from passlib.context import CryptContext
from jose import JWTError, jwt
from sqlalchemy.orm import Session
from starlette import status

from tesla.core.models import get_user_from_database
from tesla.core.schemas.user_schemas import UserRequestModel

SECRET_KEY = os.environ.get('SECRET_KEY', 'my_secret_key')
ALGORITHM = "HS256"
ACCESS_TOKEN_EXPIRE_MINUTES = 30

pwd_context = CryptContext(schemes=['bcrypt'], deprecated="auto")
oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")


def hash_password(password):
    """
        Хэширование пароля
        :param: password
        :return: hash
    """
    return pwd_context.hash(password)


def verify_password(password, password_hash):
    """
        Сравнение правильности пароля
        :param: password, password_hash
        :return: verify
    """
    return pwd_context.verify(password, password_hash)


def authenticate_user(username: str, password: str, session: Session):
    user = get_user_from_database(UserRequestModel(username=username), session)
    if not user:
        return False
    if not verify_password(password, user.password):
        return False
    return user


def create_access_token(uid: int):
    expire = datetime.utcnow() + timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
    data = {'id': uid,
            "exp": expire}
    encoded_jwt = jwt.encode(data, SECRET_KEY, algorithm=ALGORITHM)
    return encoded_jwt


async def get_current_user(session: Session, token: str = Depends(oauth2_scheme)):
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )
    try:
        payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
        uid: str = payload.get("id")
        if uid is None:
            raise credentials_exception
    except JWTError:
        raise credentials_exception
    user = get_user_from_database(UserRequestModel(id=uid), session)
    if user is None:
        raise credentials_exception
    return user
