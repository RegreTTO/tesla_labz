from typing import List

from sqlalchemy.orm import Session

from tesla.core.models import get_all_users_from_database, get_user_from_database
from tesla.core.schemas.user_schemas import UserModelGet, UserRequestModel
from tesla.core.store.db_model import User


def get_all_users_controller(session: Session) -> List[User]:
    users = get_all_users_from_database(session=session)
    return users


def get_user_by_id_controller(user_id: int, session: Session) -> User:
    user = get_user_from_database(UserRequestModel(id=user_id), session)
    return user
