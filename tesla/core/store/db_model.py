from sqlalchemy import String, Column, Integer, ForeignKey, DateTime, JSON, Float
from sqlalchemy.orm import declarative_base, relationship

base = declarative_base()


class User(base):
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True)
    name = Column(String)
    surname = Column(String)
    rendering_name = Column(String)
    email = Column(String)
    password = Column(String)
    address = Column(String)
    orders = relationship('Order', backref='user', cascade='all, delete')


class Product(base):
    __tablename__ = 'products'
    id = Column(Integer, primary_key=True)
    name = Column(String)
    description = Column(String)
    price_modifiers = Column(JSON)
    price = Column(Float)
    orders = relationship('OrderAndProductConnection', backref='product', cascade='all, delete')


class Order(base):
    __tablename__ = 'orders'
    id = Column(Integer, primary_key=True)
    author_id = Column(Integer, ForeignKey("users.id"))
    email = Column(String)
    data = Column(DateTime)
    address = Column(String)
    price = Column(Float)
    state = Column(String)
    content = relationship('OrderAndProductConnection', backref='order', cascade='all, delete')


class OrderAndProductConnection(base):
    __tablename__ = 'order_and_product_connections'
    id = Column(Integer, primary_key=True)
    order_id = Column(Integer, ForeignKey('orders.id'))
    product_id = Column(Integer, ForeignKey('products.id'))
    price = Column(Float)
